:doctype: book
:source-highlighter: coderay
:listing-caption: Listing
:pdf-page-size: A4
:toc:
:toc-placement: left
:toclevels: 4
:icons: font
:sectnums:
include::include/variablesvar.ad[] 
++++
<link rel="stylesheet"  href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.1.0/css/font-awesome.min.css">
<link rel="stylesheet"  href="include/our.css">
++++

include::include/header.ad[]
link:trainingpart1module6.html[Previous {modulename}] - link:trainingpart1module8.html[Next {modulename}]


= {modulename} 7: [.titlemodule]#{module7Title}#

In this module we'll give you a bird's-eye view on the broader UBports community that you're taking part in as an Ubuntu Touch app developer.

== Introduction

If you're developing apps for Ubuntu Touch, you're involved in a whole ecosystem and community. That's why we're taking a step back from programming in this module, to give you an idea of how the UBports community works and how you can fit in as an app developer.

We'll tell you something about the history of the UBports project and Ubuntu Touch and about the communication channels the community is using. We'll also show you where you can get help while developing your app, how you can promote your app and where you can find inspiration for app ideas.

== A short history of the UBports project

Canonical, the developer of the popular Linux distribution Ubuntu for desktops and servers, started the Ubuntu Touch project in 2011 with the goal to run on smartphones, tablets, and smart TVs. In 2013 it released the first version of Ubuntu Touch, primarily supporting the Galaxy Nexus and Nexus 4 phones.

In the mean time, Marius Gripsgård started the https://ubports.com/our-story[UBports community] and ported Ubuntu Touch to the Fairphone 2. And when Canonical stopped the project in 2017 because it had to focus on other products, the UBports project picked up development.

2017 was also the year when the https://ubports.com/foundation/ubports-foundation[UBports Foundation] was founded. It provides financial, organizational and legal support to the whole UBports community. It also serves as an independent legal entity to which community members can contribute.

Since then, a growing community kept working on the vision of an alternative Linux-based mobile operating system. By developing Ubuntu Touch apps, you're contributing to this important vision.

== The advantages of Ubuntu Touch

Using Ubuntu Touch instead of one of the dominant mobile operating systems has a lot of advantages:

- *Freedom*: Ubuntu Touch is a free and open-source GNU/Linux based mobile operating system.
- *Full transparency*: As its source is completely open, Ubuntu Touch contains no hidden nuts and bolts, and your data are safe on your own device.
- *For everyone*: The user interface is designed with perfect usability in mind. Its intuitive controls make everyone instantly feel at home.
- *Designed to last*: With Ubuntu Touch your phone will continue to receive software updates for many years to come.
- *Community-based*: Ubuntu Touch is developed by a community of users, with the sole purpose of providing the best and most secure user experience.
- *Convergence*: Ubuntu Touch is designed primarily for mobile devices. However, if you connect your mobile device to a monitor, mouse and keyboard, it instantly transforms into a desktop experience.

The more apps are available for Ubuntu Touch, the more people will be convinced to choose Ubuntu Touch instead of their current non-free mobile operating system. So as an Ubuntu Touch app developer, you have a very important role in the community.

== Taking part in the community

So where is the UBports community active and how can you take part in it? They are everywhere! We have a lot of communication channels:

=== Telegram groups

https://telegram.org/[Telegram], a free cross-platform messaging service, is our main way of communucation. There are Telegram clients for many operating systems. On Ubuntu Touch you can use the open-source Telegram client https://open-store.io/app/teleports.ubports[TELEports].

The UBports community is organized in a lot of Telegram groups, varying from general groups to groups with a very specific purpose. Here are some interesting groups you can join:

- https://t.me/ubports[UBports supergroup]: This is our main Telegram group, where you can discuss everything related to Ubuntu Touch.
- https://t.me/ubports_news[UBports News Channel]: This is the place to be if you just want to get the latest news from the UBports community. New OTA releases, calls for testing, new versions of the UBports Installer, they are all announced here.
- Ubuntu Touch on your device: An easy way to discuss issues with Ubuntu Touch that are specific to your device is to join a device-specific Telegram group. For instance, there's https://t.me/utonvolla[Ubuntu Touch on Volla Phone] and https://t.me/utonpine[Ubuntu Touch on Pine64].
- https://t.me/UbuntuAppDevEN[UBports UT App Dev]: In this group you can discuss Ubuntu Touch app development with fellow developers and ask for guidance and tips.
- https://t.me/joinchat/Bd_29FDzIzx7p7tlKYVFFw[OpenStore]: If you're publishing your apps on OpenStore, the official app store of Ubuntu Touch, it's a good idea to join this group to be up-to-date with the latest developments.

=== Forum

The https://forums.ubports.com/[UBports Forum] is a great way discuss various topics with your fellow Ubuntu Touch users. There's a category to discuss news about Ubuntu touch and its related projects, a category to discuss and solve problems, a category to discuss development of Ubuntu Touch, categories about various devices, and there's even a category https://forums.ubports.com/category/35/app-development[App Development] if you have some questions about developing apps for Ubuntu Touch.

=== Social media

The UBports community is active on a lot of social media channels:

- Videos on https://www.youtube.com/c/UBports/[YouTube]
- Tweets on https://twitter.com/ubports[Twitter]
- Updates on https://www.facebook.com/ubports[Facebook]
- Audiocasts on https://soundcloud.com/ubports[SoundCloud]
- Messages on https://mastodon.social/@ubports[Mastodon]
- Posts on https://www.linkedin.com/company/ubports-com[LinkedIn]

If you have an account on one of these social media channels, please interact with the UBports account, let us know how we're doing and share our activities.

=== Blog

The https://ubports.com/blog/ubports-news-1[UBports News] blog is regularly updated with news items from the UBports community. Press releases, the bi-weekly newsletter, the app development newsletter, release announcements, calls for testing and much more are published here. It's a great way to stay updated on what's happening in the UBports community.

It's also a great way to get to know more about people in our community. We regularly publish https://ubports.com/blog/ubports-news-1/tag/interview-8[interviews].

=== Ubuntu Touch Devices
The https://devices.ubuntu-touch.io/[Ubuntu Touch Devices] web site is the place to go for all things device-related. Which phones and tablets are supported, what features are working, which release channels are available, where's the related device subforum and Telegram group, how do you install it and how do you report a bug? It's all on this web site, with a dedicated page for each device.

=== Documentation

The https://docs.ubports.com[official documentation] of the UBports project is a great start if you want to know more. From installing Ubuntu Touch and its daily use to advanced topics such as porting Ubuntu Touch to devices, https://docs.ubports.com/en/latest/appdev/index.html[app development] and the https://docs.ubports.com/en/latest/humanguide/index.html[human interface guidelines] for the operating system. There's a lot you can learn from reading the documentation.

=== GitHub and GitLab

Ubuntu Touch is a free and open-source code project, and all of our code is published on https://github.com/ubports[GitHub] and https://gitlab.com/ubports[GitLab]. The repositories on both web sites are the places were development is happening on the core of the operating system, the default apps, the web sites and everything related to UBports. If you want to help, open issues, react to issues, try the code, contribute to the code, ... There's a lot you can do!

== How to behave in the UBports community

Every community has its rules and common values. Those of the UBports community are written in its https://ubports.com/nl/code-of-conduct[code of conduct]. If you want to be a part of this community, read this code of conduct thoroughly. Not as a strict rulebook, but as a guideline for your online behaviour.

https://en.wikipedia.org/wiki/Ubuntu_philosophy[Ubuntu] as a philosophy is about showing humanity to one another: the word itself captures the spirit of being human.

The UBports community is a productive, happy and agile community that welcomes new ideas. We gain strength from diversity, and actively seek participation from those who enhance it.

The code of conduct exists to ensure that diverse groups collaborate to mutual advantage and enjoyment. We challenge any prejudice that jeopardizes the participation of any person in our projects.

The code of conduct also governs how we behave in public and in private whenever our project can be judged by our actions. We expect our code of conduct to be honoured by everyone who represents the project officially or informally, claims affiliation with the project, or participates directly. This also means you, as an Ubuntu Touch app developer.

So if you're participating in the UBports community and maybe have some negative experience with someone, always be respectful. Maybe that person has made a mistake but has good intentions. Everyone in our community is learning, making mistakes, and improving every day. Please be patient and understanding. We can only stay a productive community if everyone feels comfortable.

== Talk about Ubuntu Touch and your app

A great way to spread the word about Ubuntu Touch and the UBports community is talking about it on events. You can also talk about the apps you've developed!

LWN.net has a https://lwn.net/Calendar/[community event calendar], tracking events of interest to people using and developing Linux and free software. They also have a https://lwn.net/Calendar/Monthly/cfp/[deadline calendar] tracking upcoming call-for-proposals deadlines for these events.

One of these interesting events is https://fosdem.org/[FOSDEM]. Every year, thousands of developers of free and open-source software from all over the world gather at the event in Brussels, and during the COVID-19 pandemic it was organized as a virtual conference. Wait for the call for proposals for the next edition and then submit a proposal for a talk. This can even be a lightning talk, which only lasts 20 minutes.

There are many other ways to talk about Ubuntu Touch and your app. Write an article on your blog about your experiences, or talk about it on social media. Repeat your message to spread the word.

== Getting inspiration for your apps

Have you created a simple app such as our shopping list app to get a feeling about Ubuntu Touch app development and do you now want to do the real stuff? Do you want to make a difference with your app, make something really useful, something valuable for the UBports community? Great!

But what if you don't have an idea for such a killer app? We encourage you to develop an app that you'd use yourself and that you're passionate about, because this greatly improves your motivation for working on it. However, maybe you just need a little bit of inspiration.

One source of inspiration are the app stores of other mobile platforms. Have a look at the popular apps at Google's https://play.google.com/store/apps[Play Store] and Apple's https://apps.apple.com/genre/ios/id36[App Store]. Do you see an app that is missing in OpenStore? Do you want to have it on Ubuntu Touch? Or if you're still using an Android phone or an iPhone, have a look at the apps on your phone. Which ones would you like to have on Ubuntu Touch? Maybe you can try developing it?

Another source of inspiration are public APIs (application programming interfaces). In the previous module of this course we showed you how you can set up a public backend server to use in your Ubuntu Touch app. However, many services already exist and offer their data with a public API. Maybe you can write an app as a frontend on Ubuntu Touch for one of these APIs. A great list of free APIs is https://github.com/public-api-lists/public-api-lists[Public API Lists] on GitHub, ordered in categories.

== Learning by reading code

The best way to learn programming is by doing it. Just start developing, even when you lack the knowledge. Start small, read and reread documentation and tutorials, write some code, try it out, fix bugs, try again, read again, and bit by bit your app will grow.

But you can also learn a lot by reading someone else's code. Luckily most Ubuntu Touch apps on OpenStore are open-source apps, so you can peek at their source code. If you visit the app's page on the OpenStore website, you can find the link to the source code in the Info part at the bottom of the page.

You can also have a look at the source code of the https://github.com/ubports/ubuntu-touch/blob/master/CORE_APPS.md[UBports core apps]. Getting to know how these work will probably teach you a trick or two.

NOTE: Not all of these apps will be QML-only apps with JavaScript such as the one we've developed in this course. Many will be written in other programming languages, such as C++.

include::include/footer.ad[]
link:trainingpart1module6.html[Previous {modulename}] - link:trainingpart1module8.html[Next {modulename}]
